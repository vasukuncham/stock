-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 02, 2018 at 10:21 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) NOT NULL,
  `brand_active` int(11) NOT NULL DEFAULT '1',
  `brand_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `brand_active`, `brand_status`) VALUES
(15, 'ck', 1, 1),
(16, 'adidas', 1, 1),
(17, 'polo', 1, 1),
(18, 'ckc', 1, 1),
(19, '', 0, 1),
(20, '', 0, 1),
(21, 'ghjhg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cards_data`
--

CREATE TABLE IF NOT EXISTS `cards_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_no` int(32) NOT NULL,
  `cvv` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cards_data`
--

INSERT INTO `cards_data` (`id`, `card_no`, `cvv`) VALUES
(1, 1234567890, 567),
(2, 987654321, 765);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_name` varchar(255) NOT NULL,
  `categories_active` int(11) NOT NULL DEFAULT '1',
  `categories_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`categories_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categories_id`, `categories_name`, `categories_active`, `categories_status`) VALUES
(10, 'Jeans', 1, 1),
(11, 'shirts', 1, 1),
(12, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_contact` varchar(255) NOT NULL,
  `client_email` text NOT NULL,
  `sub_total` varchar(255) NOT NULL,
  `vat` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL,
  `grand_total` varchar(255) NOT NULL,
  `paid` varchar(255) NOT NULL,
  `due` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `pull_payment_type` varchar(32) NOT NULL,
  `card_type` varchar(32) NOT NULL,
  `card_no` int(32) NOT NULL,
  `cvv` int(11) NOT NULL,
  `otp` int(11) NOT NULL,
  `otp_status` varchar(32) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `order_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_date`, `client_name`, `client_contact`, `client_email`, `sub_total`, `vat`, `total_amount`, `discount`, `grand_total`, `paid`, `due`, `payment_type`, `pull_payment_type`, `card_type`, `card_no`, `cvv`, `otp`, `otp_status`, `payment_status`, `order_status`) VALUES
(1, '2017-07-13', 'Vasu K', '9581218102', 'vasu@headrun.com', '400.00', '52.00', '452.00', '1', '451.00', '450', '1.00', 3, '1', '1', 2147483647, 555, 31184, '', 1, 1),
(2, '2017-07-06', 'asdfghj', '7984651371', 'vasu@headrun.com', '1000.00', '130.00', '1130.00', '1', '1129.00', '1000', '129.00', 3, '1', '1', 2147483647, 666, 87400, '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE IF NOT EXISTS `order_item` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `order_item_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`order_item_id`, `order_id`, `product_id`, `quantity`, `rate`, `total`, `order_item_status`) VALUES
(14, 24, 9, '1', '1000', '1000.00', 2),
(15, 25, 9, '1', '1000', '1000.00', 1),
(16, 26, 13, '1', '401', '401.00', 1),
(17, 37, 9, '1', '1000', '1000.00', 1),
(18, 53, 12, '1', '400', '400.00', 1),
(19, 54, 13, '1', '401', '401.00', 1),
(20, 55, 12, '1', '400', '400.00', 1),
(21, 56, 9, '1', '1000', '1000.00', 1),
(22, 57, 9, '1', '1000', '1000.00', 1),
(23, 58, 9, '1', '1000', '1000.00', 1),
(24, 60, 9, '1', '1000', '1000.00', 1),
(25, 61, 9, '1', '1000', '1000.00', 1),
(26, 62, 9, '1', '1000', '1000.00', 1),
(27, 1, 12, '1', '400', '400.00', 1),
(28, 2, 9, '1', '1000', '1000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `product_image` text NOT NULL,
  `brand_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_image`, `brand_id`, `categories_id`, `quantity`, `rate`, `active`, `status`) VALUES
(9, 'ck jeans', '../assests/images/stock/99445955d1e7c76e6.jpg', 15, 10, '-4', '1000', 1, 1),
(12, 'testing', '', 15, 10, '2', '400', 1, 1),
(13, 'testing1', '', 15, 10, '4', '401', 1, 1),
(14, 'testing2', '', 15, 10, '7', '402', 1, 1),
(15, 'testing3', '', 15, 10, '8', '403', 1, 1),
(16, 'testing4', '', 15, 10, '9', '404', 1, 1),
(17, 'testing5', '', 15, 10, '10', '405', 1, 1),
(18, 'testing9', '', 0, 10, '5', '400', 1, 1),
(19, 'testing9', '', 18, 10, '5', '400', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
