<?php 	
require_once 'core.php';


if(isset($_POST['importSubmit'])){
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                //check whether member already exists in database with same email
                $prevQuery = "SELECT brand_id FROM brands WHERE brand_name = '".trim($line[3])."'";
                $prevResult = $connect->query($prevQuery);
                $prevQuery1 = "SELECT categories_id FROM categories WHERE categories_name= '".trim($line[4])."'";
                $prevResult1 = $connect->query($prevQuery1);
                $row = $prevResult->fetch_assoc();
                $row1 = $prevResult1->fetch_assoc();
                $last_bid=$row['brand_id'];
                $last_cid =$row1['categories_id'];
                if($prevResult->num_rows <= 0){
                    //insert member data into database
                    $connect->query("INSERT INTO brands (brand_name) VALUES ('".trim($line[3])."')");
                    $last_bid = $connect->insert_id;
                }
                if($prevResult1->num_rows <= 0){
                    //insert member data into database
                    $connect->query("INSERT INTO categories (categories_name) VALUES ('".trim($line[4])."')");
                    $last_cid = $connect->insert_id;
                }
                    $connect->query("INSERT INTO product (product_name, brand_id, categories_id, quantity, rate) VALUES ('".$line[0]."','".$last_bid."','".$last_cid."','".$line[1]."','".$line[2]."')");
            }
            
            //close opened csv file
            fclose($csvFile);
        }
    }
	$connect->close();

 header("Location: http://visualpathit.com/PHP_PROJECTS/stock/product.php");
} // /if $_POST
