<?php 	

require_once 'core.php';

function send_mail($to_id, $otp_no){
    $from = "vmounika16@gmail.com";
    $to = $to_id;
    $subject = "OTP Verification For Stock Project";
    $from_name = "Verification";

    $sender_message = "<html><head></head><body>".
                        "<p>Hi,</p>".
                        "<p>Here is your secret one time payment no : <strong>".$otp_no."</strong></p>";
    $boundary = md5("sanwebe.com"); 

    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "From: ".$from_name." <".$from.">\r\n"; 
    $headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n"; 

    $body = "--$boundary\r\n";
    $body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $body .= "Content-Transfer-Encoding: base64\r\n\r\n"; 
    $body .= chunk_split(base64_encode($sender_message)); 

    // now send the email
    $retval = @mail($to, $subject, $body, $headers);
    return $retval;
}

$valid['success'] = array('success' => false, 'messages' => array(), 'order_id' => '');
// print_r($valid);
if($_POST) {	
	//print_r($_POST); die;

  $orderDate 				= date('Y-m-d', strtotime($_POST['orderDate']));	
  $clientName 				= $_POST['clientName'];
  $clientContact 			= $_POST['clientContact'];
  $subTotalValue 			= $_POST['subTotalValue'];
  $vatValue 				= $_POST['vatValue'];
  $totalAmountValue         = $_POST['totalAmountValue'];
  $discount 				= $_POST['discount'];
  $grandTotalValue 			= $_POST['grandTotalValue'];
  $paid 					= $_POST['paid'];
  $dueValue 				= $_POST['dueValue'];
  $paymentType 				= $_POST['paymentType'];
  $pullPayment 				= $_POST['pullPayment'];
  $pullPaymentcard 			= $_POST['pullPaymentcard'];
  $cno 						= $_POST['cno'];
  $cvv 						= $_POST['cvv'];
  $paymentStatus 			= $_POST['paymentStatus'];
  $clientEmail				= $_POST['clientEmail'];
  $otp_no					= rand(10000,99999);
  $cur_domain				= apache_request_headers();
				
	$sql = "INSERT INTO orders (order_date, client_name, client_contact, client_email,  sub_total, vat, total_amount, discount, grand_total, paid, due, payment_type, pull_payment_type, card_type, card_no, cvv, otp, otp_status, payment_status, order_status) VALUES ('$orderDate', '$clientName', '$clientContact','$clientEmail', '$subTotalValue', '$vatValue', '$totalAmountValue', '$discount', '$grandTotalValue', '$paid', '$dueValue', $paymentType, '$pullPayment', '$pullPaymentcard', '$cno', '$cvv', '$otp_no', '', $paymentStatus, 1)";
	
	$order_id;
	$orderStatus = false;
	if($connect->query($sql) === true) {
		$order_id = $connect->insert_id;
		$valid['order_id'] = $order_id;
		$valid['cno'] = $cno;
		$valid['cvv'] = $cvv;
		$valid['otp_send'] = false;

		//curl request goes here to constant API
		/*$curl_post_data = array('cno'=> $cno,
								'cvv'=> $cvv,
								'otp_no'=> $otp_no);
		$api_url = $cur_domain['Origin']."/thirdparty_service.php?".http_build_query($curl_post_data);
		//print_r($api_url); die;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $api_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, false);
		$curl_response =  json_decode(curl_exec($curl));*/
		//if($pullPayment==1){
			$otp_send = send_mail($clientEmail, $otp_no);
			if ($otp_send) {
				$valid['otp_send'] = true;	
			}
		//}
		if ($curl_response) {
				
		}
		$orderStatus = true;
	}

		
	// echo $_POST['productName'];
	$orderItemStatus = false;

	for($x = 0; $x < count($_POST['productName']); $x++) {			
		$updateProductQuantitySql = "SELECT product.quantity FROM product WHERE product.product_id = ".$_POST['productName'][$x]."";
		$updateProductQuantityData = $connect->query($updateProductQuantitySql);
		
		
		while ($updateProductQuantityResult = $updateProductQuantityData->fetch_row()) {
			$updateQuantity[$x] = $updateProductQuantityResult[0] - $_POST['quantity'][$x];							
				// update product table
				$updateProductTable = "UPDATE product SET quantity = '".$updateQuantity[$x]."' WHERE product_id = ".$_POST['productName'][$x]."";
				$connect->query($updateProductTable);

				// add into order_item
				$orderItemSql = "INSERT INTO order_item (order_id, product_id, quantity, rate, total, order_item_status) 
				VALUES ('$order_id', '".$_POST['productName'][$x]."', '".$_POST['quantity'][$x]."', '".$_POST['rateValue'][$x]."', '".$_POST['totalValue'][$x]."', 1)";

				$connect->query($orderItemSql);		

				if($x == count($_POST['productName'])) {
					$orderItemStatus = true;
				}		
		} // while	
	} // /for quantity

	$valid['success'] = true;
	$valid['otp_no'] = $otp_no;
	$valid['messages'] = "Successfully Added";		
	
	$connect->close();

	echo json_encode($valid);
 
} // /if $_POST
// echo json_encode($valid);